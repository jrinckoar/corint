#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='corint',
      version='0.2',
      description='Correlation Integral Python Codes',
      long_description=readme(),
      url='https://jrinckoar@bitbucket.org/jrinckoar/corint.git',
      author='Juan F. Restrepo',
      author_email='jrestrepo@bioingenieria.edu.ar',
      license='MIT',
      packages=['corint'],
      keywords='correlation-integral gaussian-correaltion-integral \
                u-correlation-integral',
      install_requires=[
          'scipy',
          'numpy',
          'matplotlib',
          'joblib',
          'tqdm',
          'pywavelets',
      ],
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      zip_safe=False)
