============
CORINT
============
Juan F. Restrepo: jrestrepo@bioingenieria.edu.ar


Compilation of python routines to compute the correlation integral:

1. Grassberger and Procaccia correlation integral.
2. Gaussian correlation integral.
3. U-correlation integral.

Bibliography
------------------
1.  "@article{Grassberger1983b,
    author = {Grassberger, Peter and Procaccia, Itamar},
    journal = {Physical Review Letters},
    pages = {346--349},
    title = {Characterization of strange attractors},
    volume = {50},
    year = {1983}}"
2.  "@article``{Diks1996,
    author = {Diks, C},
    journal = {Physical Review E},
    number = {5},
    pages = {R4263--R4266},
    title = {Estimating invariants of noisy attractors},
    volume = {53},
    year = {1996}}"
3.  "@article{Restrepo2017,
    author  = {Restrepo, Juan F.and  Schlotthauer, Gastón},
    title   = {Automatic estimation of attractor invariants},
    journal = {Nonlinear Dynamics (submitted)},
    year    = {2017}}"
