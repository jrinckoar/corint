#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
scipy.special.gammaincc: Incomplete Regularized Gamma Function.
'''
import sys
import numpy as np
from scipy.special import gammaincc         # Incomplete Upper Gamma function
from scipy.special import gamma             # Gamma function
import scipy.interpolate as ip              # Data interpolator
import matplotlib.pyplot as plt
from corint.corint_lib import delayVectors, distanceVec, wavediff
from joblib import Parallel, delayed
import multiprocessing
from tqdm import tqdm


def uci(x, m, beta, h, tau=1, norm='euclidean2', Tn=1, cores_=8, pbar=True):

    x = np.longdouble(x)
    h = np.longdouble(h)
    M = len(m)
    H = len(h)
    uci = np.zeros((H, M))
    uci2 = np.zeros((H, M))
    h = h**2
    # Data normalization
    x = (x - np.mean(x)) / np.std(x)

    t = 10**np.concatenate(([-10], np.linspace(-8, 3, 300), [10]))
    with Parallel(n_jobs=cores_) as parallel:
        for i in tqdm(range(M), file=sys.stdout, disable=not pbar):
            dvec = delayVectors(x, m[i], tau)
            Z = distanceVec(dvec, norm, Tn=Tn)
            K0 = ip.InterpolatedUnivariateSpline(t, gammaincc(beta[i] / 2, t))
            par = parallel(delayed(Uci_eval)
                           (z=Z, kernel=K0, b=beta[i] / 2 - 1,
                            g=gamma(beta[i] / 2), h=j) for j in h)
            uci[:, i], uci2[:, i] = zip(*par)
    return uci, uci2


def Uci_eval(z, kernel, b, g, h):
    x = z / h
    u = np.mean(kernel(x))
    u2 = u - (1 / g) * np.mean(np.exp(-x) * (x)**b)
    return u, u2


if __name__ == '__main__':

    N = 2000
    m = [3, 4, 5, 6]
    beta = m
    tau = 1
    h = np.exp(np.linspace(-8, 2, 100))

    x = np.zeros(N + 500)
    x[0] = np.random.random(1)
    for i in range(1, N + 500):
        x[i] = 3.86 * x[i - 1] * (1 - x[i - 1])
    x = x[500:-1]

    UCI, UCI2 = uci(x, m, beta, h, tau=1)

    D = np.tile(h, (len(m), 1)).T * \
        (wavediff(UCI) / wavediff(np.tile(h, (len(m), 1)).T)) / UCI

    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((1, 2), (0, 0))
    ax2 = plt.subplot2grid((1, 2), (0, 1))

    ax1.plot(np.log(h), np.log(UCI), 'b', label='Uci')
    ax1.plot(np.log(h), np.log(UCI2), 'g', label='Uci2')
    ax2.plot(np.log(h), D, 'b')

    ax1.set_ylabel('ln(UCI)')
    ax1.set_xlabel('ln(h)')
    ax2.set_ylabel('D')
    ax2.set_xlabel('ln(h)')
    plt.suptitle('Logistic Map U-Correlation Integral')
    plt.show()
