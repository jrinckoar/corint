#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import pywt
import numpy as np
from matplotlib import pyplot as plt


def delayVectors(x, m, tau):
    m = int(m)
    L = len(x) - (m - 1) * tau
    sVec = np.zeros((L, m))
    k = (m - 1) * tau
    for i in range(L):
        sVec[i, :] = x[i: i + k + 1: tau]
    return sVec


def distanceVec(sVec, norm='euclidean2', Tn=0):

    V = np.array(sVec)
    Q = V.shape[0]
    L = int((Q - Tn) * (Q - Tn - 1) / 2)
    Z = np.zeros(L)
    c = 0
    for k in range(1, Q - Tn):
        v1 = V[0:Q - Tn - k, :]
        v2 = V[k + Tn:Q, :]
        if norm == 'euclidean':
            a = np.sum(v1**2, axis=1)
            b = np.sum(v2**2, axis=1)
            ab = np.sum(v1 * v2, axis=1)
            z = np.sqrt(a + b - 2 * ab)
        elif norm == 'euclidean2':
            a = np.sum(v1**2, axis=1)
            b = np.sum(v2**2, axis=1)
            ab = np.sum(v1 * v2, axis=1)
            z = a + b - 2 * ab
        elif norm == 'max':
            z = np.max(np.abs(v1 - v2), axis=1)
        i = len(z)
        Z[c:c + i] = z
        c = c + i
    return Z


def surrogate(x):
    Fx = np.fft.rfft(x)
    t = np.random.normal(0, 1, len(x))
    Ft = np.fft.rfft(t)
    return np.fft.irfft(np.sqrt((Fx.real**2 + Fx.imag**2)) *
                        np.exp(Ft.imag * 1.0j))


def wavediff(y, scale=2):

    dydt = np.zeros(y.shape)
    K = ((2 * np.pi)**(1 / 4)) * scale**(3 / 2)
    cols = 1
    if y.ndim > 1:
        _, cols = y.shape
        for i in range(cols):
            yw, _ = pywt.cwt(y[:, i], scale, 'gaus1')
            dydt[:, i] = yw / K
    else:
        yw, _ = pywt.cwt(y, scale, 'gaus1')
        dydt = yw / K

    return np.squeeze(-dydt)


def test_stateVectors():
    x = np.linspace(0, 5, 6)
    m_ = 2
    tau_ = 1
    Tn_ = 2
    dVec = delayVectors(x, m=m_, tau=tau_)
    V = distanceVec(dVec, Tn=Tn_)
    print('Series:\n {0}'.format(x))
    print('Delay Vectors m={0}, tau={1},  Tn={2}:\n {3}'.
          format(m_, tau_, Tn_, dVec))
    print('Distance Vector:\n {0}'.format(V))


def test_wavediff():
    N = 1000
    sc = 8
    t = np.linspace(0, 16 * np.pi, N)
    y = np.random.normal(0, 1, (N, 2))
    dy = wavediff(y, scale=sc)
    dt = wavediff(t, scale=sc)

    plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((1, 2), (0, 0))
    ax2 = plt.subplot2grid((1, 2), (0, 1))

    ax1.plot(t, y, 'b', label='y')
    ax2.plot(t, dy / np.tile(dt, (2, 1)).T, 'r', label='dy')
    plt.suptitle('Wavelet derivative')
    plt.show()


if __name__ == '__main__':

    test_stateVectors()
    # test_wavediff()
