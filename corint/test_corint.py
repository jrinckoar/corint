#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
from corint_lib import wavediff
from gci import gci
from uci import uci


N = 2000
m = [3, 4, 5, 6]
beta = m
tau = 1
h = np.exp(np.linspace(-8, 2, 100))

x = np.zeros(N + 500)
x[0] = np.random.random(1)
for i in range(1, N + 500):
    x[i] = 3.86 * x[i - 1] * (1 - x[i - 1])
x = x[500:-1]


GCI = gci(x, m, h, tau=1)
UCI, UCI2 = uci(x, m, beta, h, tau=1)


Dg = np.tile(h, (len(m), 1)).T * \
    (wavediff(GCI) / wavediff(np.tile(h, (len(m), 1)).T)) / GCI

Du = np.tile(h, (len(m), 1)).T * \
    (wavediff(UCI) / wavediff(np.tile(h, (len(m), 1)).T)) / UCI

fig = plt.figure(figsize=plt.figaspect(0.5))
ax1 = plt.subplot2grid((1, 2), (0, 0))
ax2 = plt.subplot2grid((1, 2), (0, 1))

l1 = ax1.plot(np.log(h), np.log(GCI), 'b')[0]
l2 = ax1.plot(np.log(h), np.log(UCI), 'r')[0]
ax1.set_xlabel('ln(h)')

ax2.plot(np.log(h), Dg, 'b', np.log(h), Du, 'r')[0:2]
ax2.set_ylabel('D')
ax2.set_xlabel('ln(h)')

plt.suptitle('Logistic Map GCI and UCI correlation dimension')
fig.legend((l1, l2), ('GCI', 'UCI'), 'upper left')

plt.show()
