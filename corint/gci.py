#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import sys
import numpy as np
import matplotlib.pyplot as plt
from corint.corint_lib import delayVectors, distanceVec, wavediff
from joblib import Parallel, delayed
import multiprocessing
from tqdm import tqdm


def gci(x, m, h, tau=1, norm='euclidean2', cores_=8, pbar=True):

    x = np.longdouble(x)
    h = np.longdouble(h)
    M = len(m)
    H = len(h)
    h = h**2
    gci = np.zeros((H, M))

    # Data normalization
    x = (x - np.mean(x)) / np.std(x)

    with Parallel(n_jobs=cores_) as parallel:
        for i in tqdm(range(M), file=sys.stdout, disable=not pbar):
            dvec = delayVectors(x, m[i], tau)
            V = (-1) * distanceVec(dvec, norm)
            gci[:, i] = parallel(delayed(Gci_eval)(z=V, h=j) for j in h)
    return gci


def Gci_eval(z, h):
    return np.mean(np.exp(z / h))


if __name__ == '__main__':

    N = 2000
    m = [3, 4, 5, 6]
    tau = 1
    h = np.exp(np.linspace(-8, 2, 100))

    x = np.zeros(N + 500)
    x[0] = np.random.random(1)
    for i in range(1, N + 500):
        x[i] = 3.86 * x[i - 1] * (1 - x[i - 1])
    x = x[500:-1]

    GCI = gci(x, m, h, tau=1)

    D = np.tile(h, (len(m), 1)).T * \
        (wavediff(GCI) / wavediff(np.tile(h, (len(m), 1)).T)) / GCI

    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((1, 2), (0, 0))
    ax2 = plt.subplot2grid((1, 2), (0, 1))

    ax1.plot(np.log(h), np.log(GCI), 'b', label='Uci')
    ax2.plot(np.log(h), D, 'b')

    ax1.set_ylabel('ln(GCI)')
    ax1.set_xlabel('ln(h)')
    ax2.set_ylabel('D')
    ax2.set_xlabel('ln(h)')
    plt.suptitle('Logistic Map Gaussian-Correlation Integral')
    plt.show()
