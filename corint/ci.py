#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-


import sys
import numpy as np
import matplotlib.pyplot as plt
from corint.corint_lib import delayVectors
from corint.corint_lib import distanceVec
from joblib import Parallel, delayed
import multiprocessing
from tqdm import tqdm


def ci(x, m, tau, h, norm='euclidean2', cores_=4, pbar=False):
    M = len(m)
    H = len(h)
    if norm == 'euclidean2':
        h = h**2
    ci = np.zeros((H, M))
    with Parallel(n_jobs=cores_) as parallel:
        for i in tqdm(range(M), file=sys.stdout, disable=pbar):
            dvec = delayVectors(x, m[i], tau)
            V = (-1) * distanceVec(dvec, norm)
            ci[:, i] = parallel(delayed(Ci_eval)(z=V, h=j) for j in h)
    return ci


def Ci_eval(z, h):
    return np.mean((1 + z / h) >= 0)


if __name__ == '__main__':
    N = 3000
    m = [2, 3, 4, 5, 6]
    h = np.exp(np.linspace(-7, 0, 300))
    x = np.zeros(N + 500)
    x[0] = np.random.random(1)
    for i in range(1, N + 500):
        x[i] = 3.99 * x[i - 1] * (1 - x[i - 1])
    x = x[500:-1]
    CI = ci(x, m, 1, h)

    ht = np.transpose(np.tile(h, (len(m), 1)))
    D_dif = (np.diff(CI, axis=0) / np.diff(ht, axis=0))
    D = ht[:-1, :] * D_dif / CI[:-1, :]

    fig = plt.figure(figsize=plt.figaspect(0.5))
    ax1 = plt.subplot2grid((1, 2), (0, 0))
    ax2 = plt.subplot2grid((1, 2), (0, 1))

    ax1.plot(np.log(h), np.log(CI))
    ax2.plot(np.log(h[:-1]), D)

    ax1.set_ylabel('ln(CI)')
    ax1.set_xlabel('ln(h)')
    ax2.set_ylabel('D')
    ax2.set_xlabel('ln(h)')
    plt.suptitle('Logistic Map Grassberguer-Procassia Correlation Integral')
    plt.show()
